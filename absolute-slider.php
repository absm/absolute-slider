<?php
/**
 * @package Absolute-Slider
 * @author Byju John, Absolute Media
 */
 
/*
Plugin Name: Absolute-Slider
Plugin URI: http://www.absolutemedia.co.uk
Description: A plugin to read a specified folder based on the post id and generate the a list of images, which could be used for JavaScript Sliders  
Version: 1.0
Author: Byju John, Absolute Media
License: Not for public use
*/

//add_action('init','abosolute_slider');
function abosolute_slider()
{
	global $post;
	
	$postfolder = 'default';
	if($post->ID > 0) {
		$postfolder = $post->ID;
	}
	if(!is_page()) {
		$postfolder = 'posts';
	}
	
	$dir = 'img/' . $postfolder;
	
	if (!is_dir($dir))	{
		$postfolder = 'default';
		$dir = 'img/' . $postfolder;
	}
	
	$type = 'jpg';
	
	$files	= array();
	$images	= array();
	
	if (is_dir($dir))
	{
		if ($handle = opendir($dir)) {
			while (false !== ($file = readdir($handle))) {
				if ($file != '.' && $file != '..' && $file != 'CVS' && $file != 'index.html') {
					$files[] = $file;
				}
			}
		}
		closedir($handle);
	
		$i = 0;
		foreach ($files as $img)
		{
			if (!is_dir($dir . '/' . $img))
			{
				if (preg_match('/'.$type.'/', $img)) {
					$images[$i]->name	= $img;
					$images[$i]->fullname	= site_url() . '/'. $dir .'/' . $img ;
					$size = getimagesize (ABSPATH . $dir . '/' . $images[$i]->name);
					$images[$i]->width 	= $size[0];
					$images[$i]->height	= $size[1];
					
					$images[$i]->link = get_post_meta($post->ID, basename ( $img, '.'.$type ), true);
					$i++;
				}
			}
		}
	
	}
	
	foreach($images as $img) {
		echo $img->link > '' ? '<a href="'.$img->link . '" >' : '';
		echo '<img src="' . $img->fullname . '" width="'.$img->width.'" height="'.$img->height.'" alt="'.$img->name.'">';
		echo $img->link > '' ? '</a>' : '';
	}	
	
	
}


?>